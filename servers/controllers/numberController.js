var Sequelize = require("sequelize");
var Numbers = require("../models/numbers");
var sequelize = require("../../config/db");
const Op = Sequelize.Op;
const io = require('../../app2').io;  

 const numbers_tb = async (req, res) => {
    const response = await sequelize
      .sync()
      .then(function () {
        const data = Numbers.findAll();
        console.log("connection connected");
        return data;
      })
      .catch((err) => {
        return err;
        console.log("connection connected");
      });
    res.json(response);
  };

  const get = async(req,res)=>{
    res.sendFile(__dirname+"/index.html")
  }

  const send = async(req,res)=>{
        let {number,name,lastname,address,password} = req.body;
        let randomNumber = Math.floor(1000+Math.random() * 9000);
        let io = req.app.get("io");
      // console.log(code,number)
      let FoundUser ;
    await Numbers.findOne({where:{phoneNumber:number}}).then((data)=>{
      FoundUser = data;
     })
      
     if(!FoundUser){
          Numbers.create({
            code:randomNumber,
            phoneNumber:number,
            send:false,
            name,
            lastname,
            address,
            password
          }).then((data)=>{
              res.json(data);
              Numbers.findAll({where:{send:false}})
              .then((data)=>{
                  io.emit("data",data);
                  })
              .catch((err)=>console.log(err));
          }).catch((err)=>{
            console.log(err);
          })
     }else{
        res.json(FoundUser)
     }
  }

  const Update = async(req,res)=>{
    let io = req.app.get("io");
    let {number,code} = req.body;
    let FoundUser 
    await Numbers.findOne({where:{phoneNumber:number}}).then((data)=>{
      FoundUser = data;
     })
    if(FoundUser){
      console.log(code,"=",FoundUser.code)
        if(FoundUser.code == code){
            Numbers.destroy(
              {
              where:{phoneNumber:number}
            }).then((data)=>{
              res.json("ok")
              Numbers.findAll({where:{send:false}})
                .then((data)=>{
                    io.emit("data",data);
                    })
                .catch((err)=>console.log(err));
              
              
            }).catch(err=>console.log(err));
        }else{
          res.send("code yalnysh")
        }
    }else{
      res.send("Deleted!")
    }
  }
 
  const deleteAll = async(req,res)=>{
    Numbers.destroy({
      where:{
        send:true
      }
    }).then((data)=>{
      res.json(data);
    }).catch((err)=>{
      console.log(err);
    })
  }
  exports.numbers_tb = numbers_tb;
  exports.send = send;
  exports.get = get;
  exports.Update = Update;
  exports.deleteAll = deleteAll;