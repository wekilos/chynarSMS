var Sequelize = require("sequelize");
var Notifications = require("../models/notification");
var sequelize = require("../../config/db");
const Op = Sequelize.Op;
const io = require('../../app2').io;  

 const notifications_tb = async (req, res) => {
    const response = await sequelize
      .sync()
      .then(function () {
        const data = Notifications.findAll();
        console.log("connection connected");
        return data;
      })
      .catch((err) => {
        return err;
      });
    res.json(response);
  };

  
  const get = async(req,res)=>{

    Notifications.findAll().then((data)=>{
      res.json(data)
    }).catch((err)=>{
      console.log(err);
    })

  }

  const create = async(req,res)=>{
        let {title,description} = req.body;
        let io = req.app.get("io");
      // console.log(code,number)
      
      Notifications.create({
            title,
            description,
          }).then((data)=>{
              res.json(data);
              Notifications.findAll()
              .then((data)=>{
                  let newData=[];
                  data.map((dat)=>{
                    let date = dat.createdAt.getTime();
                    let today =  new Date().getTime();
                    let day = 86476000;
                    if(today-date>=day*10){
                      console.log("10 gun boldy")
                    }else{
                      newData.push(dat);
                    }
                  })
                  
                io.emit("notifications",newData);
                  })
              .catch((err)=>console.log(err));
          }).catch((err)=>{
            console.log(err);
          })
     
  }

  const Delete = async(req,res)=>{
    let {id} = req.params;
    let FoundNotification; 
    await Notifications.findOne({where:{id:id}}).then((data)=>{
      FoundNotification = data;
     })
    if(FoundNotification){
      Notifications.destroy({
              where:{id:id}
            }).then(async(data)=>{
               await Notifications.findAll()
                .then((data)=>{
                  let newData = data.filter((e)=>{
                    return e.id!==FoundUser.id
                  }); 
                    io.emit("notifications",newData);
                })
                .catch((err)=>console.log(err));
              
              res.json("ok")
            }).catch(err=>console.log(err));
        
    }else{
      res.send("Deleted!")
    }
  }
 
  exports.notifications_tb = notifications_tb;
  exports.create = create;
  exports.Delete = Delete;
  exports.get = get;