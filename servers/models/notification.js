// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

var Notifications = sequelize.define(
    "Notification",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        title:Sequelize.STRING,
        description:Sequelize.TEXT,
    },
    {
        timestamps: true,
    }
);



module.exports = Notifications;