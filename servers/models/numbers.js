// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

var Numbers = sequelize.define(
    "Number",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        phoneNumber:Sequelize.BIGINT,
        code:Sequelize.INTEGER,
        send:Sequelize.BOOLEAN,
        name:Sequelize.STRING,
        lastname:Sequelize.STRING,
        address:Sequelize.STRING,
        password:Sequelize.STRING,
    },
    {
        timestamps: true,
    }
);



module.exports = Numbers;