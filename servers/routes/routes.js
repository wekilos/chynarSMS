const express = require("express");
// const { verify } = require("crypto");
const Func = require("../functions/functions");
const sequelize = require("../../config/db");
const router = express.Router();
const jwt = require("jsonwebtoken");



// Controllers
const NumbersControllers = require("../controllers/numberController");
const Notifications = require("../controllers/notificationsController");

router.get("/numbers_tb",NumbersControllers.numbers_tb);
// router.get("/notifications_tb",Notifications.notifications_tb);



// // Routes
router.get("/",NumbersControllers.get);
router.post("/send",NumbersControllers.send);
router.post("/after/send",NumbersControllers.Update);
router.delete("/delete",NumbersControllers.deleteAll);

// Notifications
// router.get("/notifications",Notifications.get);
// router.post("/create/notification",Notifications.create);
// router.post("/delete/notification/:id",Notifications.Delete)


// For Token

function verifyToken(req, res, next) {
    const bearerHeader =
      req.headers["authorization"] || req.headers["Authorization"];
    if (typeof bearerHeader !== "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
  
      jwt.verify(bearerToken, Func.Secret(), (err, authData) => {
        if (err) {
          res.json("err");
          console.log(err);
          
        } else {
          req.id = authData.id;
        }
      });
      next();
    } else {
      res.send("<center><h2>This link was not found! :(</h2></center>");
    }
  }
  
  module.exports = router;