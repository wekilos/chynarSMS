const express = require('express');
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const https = require('https');
const http = require('http');

const fs = require("fs");
// https uchin son achmaly
var privateKey  = fs.readFileSync('/etc/letsencrypt/live/cynar.com.tm/private.key', 'utf8');
var certificate = fs.readFileSync('/etc/letsencrypt/live/cynar.com.tm/sum6.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

var httpServer = http.createServer(app);
// htpps uchin son achmaly
var httpsServer = https.createServer(credentials, app);

// const server = https.createServer(credentials,app);
const { Server } = require("socket.io");
const io = new Server();
io.attach(httpServer);
io.attach(httpsServer);
var Sequelize = require("sequelize");
const Numbers = require("./servers/models/numbers");
const Notifications = require('./servers/models/notification');

app.set('io', io);
app.use(bodyParser.json({limit: "1000mb"}));
app.use(bodyParser.urlencoded({limit: '1000mb', extended: true,parameterLimit:50000}));
app.use(
    cors({
      origin:"*"
    })
  );

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
  
    res.setHeader("Allow", "GET, POST, OPTIONS, PUT, DELETE");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
    );
    next();
  });

  const Routers = require("./servers/routes/routes");
const { DATE, DATEONLY } = require('sequelize');

  app.use("/api", Routers);

  app.use("/", (req, res) => {
    res.send(404);
  });

  io.on('connection', (socket) => {
    console.log(' user connected');

          Numbers.findAll({where:{send:false}}).then((data)=>{
            // console.log(data)
          socket.emit("data",data);
        }).catch((err)=>console.log(err));
  
        Notifications.findAll().then((data)=>{
          let newData=[];
          data.map((dat)=>{
            let date = dat.createdAt.getTime();
            let today =  new Date().getTime();
            let day = 86476000;
            if(today-date>=day*10){
              console.log("10 gun boldy")
            }else{
              newData.push(dat);
            }
          })
          
        socket.emit("notifications",newData);
      }).catch((err)=>console.log(err));

    socket.on('chat message', (msg) => {
      console.log('message: ' + msg);
    });

    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
  });

 







// FOR PORT 8080




// your express configuration here



// httpServer.listen(1111,()=>{
//   console.log("working http server on 1111 port")
// });

// // son achmaly
// httpsServer.listen(5443,()=>{
//   console.log("working https server on 5443 port")
// });

// server.listen(1111, () => {
//   console.log('listening on *:1111');
// });


var httpServer = http.createServer(app,(req,res)=>{
  res.writeHead(301, { "Location":"https://" + req.headers['host'] + req.url});
  // res.end();
});

// htpps uchin son achmaly
// var httpsServer = https.createServer(credentials, app);

httpServer.listen(1111,()=>{
  console.log("working http server on 1111 port")
  
});

// son achmaly
httpsServer.listen(5443,()=>{
  console.log("working https server on 5443 port")
});
